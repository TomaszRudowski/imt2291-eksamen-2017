<?php
/*
Når brukeren er logget på (enten ved å ha opprettet ny bruker eller logget på ved å kjøre oppgave2.php)
så skal vedkommende ved å gå til oppgave4.php få mulighet til å laste opp avatar bilde (bilde for brukeren).
 Lag en form for å la brukeren laste opp bilde. Når brukeren sender bilde så skal dette sendes til samme skriptet.
 Skaler bildet til maksimalt 150x150 punkter og lagre det i tabellen i kolonnen avatar for riktig bruker.
 */
 require_once 'db.php';  // get global PDO object
 require_once 'globalFunc.php';  // formats header and bottom

session_start();

/**
 *  formats html form to upload file, inspired by form from scaleUploadedImage.php from course materials
 *  @method addAvatar
 */
function addAvatarForm() {
  $html = '
      <form method="post" action="oppgave4.php" enctype="multipart/form-data">
        <label for="file">Velg fil: </label>
        <input type="file" id="file" name="file"/><br/>
        <input type="submit" value="Add Avatar"/>
      </form>';
  return $html;
}

/**
 *  change format to 150x150, based on scaleUploadedImage.php from course materials
 *  @method processPicture
 *  @return [type]         [description]
 */
function processPicture() {
  // start from scaleUploadedImage.php, resize uploaded picture
  $maxHeight = 150;
	$maxWidth = 150;
//	header ("Content-type: image/jpg");	// We will send back a jpg image
	$imgData = file_get_contents($_FILES['file']['tmp_name']);	// Read the uploaded file
	$img = imagecreatefromstring($imgData);		// Convert it to an image
	$width = imagesx($img);			// Find the width
	$height = imagesy($img);		// Fint the height
	$scaleX = $width/$maxWidth;
	$scaleY = $height/$maxHeight;

	$scale = ($scaleX>$scaleY)?$scaleX:$scaleY;	// What axis needs the most scaling
	$scale = ($scale>1)?$scale:1;				// Do not scale UP

	$scaled = imagecreatetruecolor($width/$scale, $height/$scale);	// Create new image with the scaled size
	// Scale the image
	imagecopyresampled($scaled, $img, 0, 0, 0, 0, $width/$scale, $height/$scale, $width, $height);
// end from scaleUploadedImage.php

  // next I need to get the picture as binary string
  // start from: https://stackoverflow.com/questions/2207865/convert-gd-image-back-to-binary-data
  ob_start();
  imagejpeg($scaled);
  $scaled_as_binary = ob_get_contents();
  ob_end_clean();
  // end from stack overflow

  updateUserAvatar($scaled_as_binary);
}

/**
 *  updete avatar field in db for user defined by $_SESSION['userID']
 *  @method updateUserAvatar
 *  @param  string           $pict picture as a binary string
 *  @return [type]                 [description]
 */
function updateUserAvatar($pict) {
  global $db;
  $sql = "UPDATE user SET avatar=? WHERE id=?";
  $stm = $db->prepare($sql);
  $stm->execute(array($pict,$_SESSION['userID']));
}

/*
Script runs as follows, test if user is logged in, if yes tests if its first time loaded page
or file was allready uploaded, based on this, shows upload form or process file
 */

if(isset($_SESSION['userID'])) {    // logged in
  if (isset($_FILES['file'])) {     // and file uploaded to tmp folder
    processPicture();
  //  echo 'file uploaded';
//    var_dump($_FILES);
  } else {                          // logged in waiting for uploading file, show form
    echo addAvatarForm();
  }
} else {
  echo 'not logged in';
}

?>
