<?php
/*
Fra http://api.geonames.org/postalCodeSearchJSON?placename=gj%C3%B8vik&country=NO&maxRows=30&username=demo
vil du få en oversikt over alle postnummer i Gjøvik. Les disse dataene og skriv ut en tabell med postnummer
og stedsnavn (placename). Ta kun med steder i Oppland. Bruk en tabell for å vise postnummer/stedsnavn.
Det er en begrensning på 2000 forespørsler pr. time pr. brukernavn. I tillegg til den globale brukeren demo
så har jeg også opprettet en bruker med brukernavn "okolloen" men også der kan antall forespørsler pr. time
gå over 2000. Gå til http://www.geonames.org/login for å opprette et eget brukernavn til bruk istedenfor "demo"/"okolloen".
 */

require_once 'globalFunc.php';  // formats header and bottom

// gets json object from file
$json_from_file = file_get_contents("http://api.geonames.org/postalCodeSearchJSON?placename=gj%C3%B8vik&country=NO&maxRows=30&username=okolloen");
// converts it to associative array
$data = json_decode($json_from_file, TRUE);
$postalCodes = $data['postalCodes'];

/*  eksempel $subArray to format in 'foreach' loop
array(10) { ["adminCode2"]=> string(4) "0502" ["adminCode1"]=> string(2) "05"
["adminName2"]=> string(7) "Gjøvik" ["lng"]=> float(10.69155)
["countryCode"]=> string(2) "NO" ["postalCode"]=> string(4) "2816"
["adminName1"]=> string(7) "Oppland" ["ISO3166-2"]=> string(2) "05"
["placeName"]=> string(7) "Gjøvik" ["lat"]=> float(60.79574) } */

echo pageHeader();

// opens table-tag and format header, bootstrap class
echo '<div><table class="table"><thead><tr><th>Postal Code</th><th>Place Name</th></tr></thead><tbody>';

foreach ($postalCodes as $index=>$subArray) {
  // compare $subArray['adminName1'] with Oppland. if true create one row in the table
  if ($subArray['adminName1'] === "Oppland") {
    echo '<tr>
        <td>'.$subArray["postalCode"].'</td>
        <td>'.$subArray["placeName"].'</td>
      </tr>';
  }
}
echo '</tbody></table></div>';  // close table-tags

echo pageBottom();
?>
