<?php
/*
I katalogen sql (i dette repositoriet) finnes filen studyPrograms.sql, importer
denne inn i databasen du laget i oppgave 1. Du har nå fått tre nye tabeller :
studyprogram : Inneholder navn på studieprogrammer.
studyprogramContent : Inneholder emner som inngår i et studieprogram. startYear
er hvilket år studentene startet, dette da innholdet i et studieprogram vil variere
avhengig av hvilket år en starter (det er ulike emner som inngår, emnene kan være plassert
i ulike semestre.) Det er også merket om et emne er obligatorisk eller et valgemne.
subject : Inneholder mer informasjon om de ulike emnene. For å koble et emne til
et studieprogram må en altså finne emne med riktig emnekode og en må ta høyde for
når emnet undervises. Hvert emne finnes altså på nytt for hvert år det er brukt i
et studieprogram. URL'en kan/vil være ulik for samme emne undervist i ulike år, dette
da innholdet i emnet kan endres uten at emnekoden endres.
Ut i fra den informasjonen som ligger i disse tabellene skal du lage oppgave8.php som
enererer en oversikt over alle studieprogram med oppstart i 2016 (alle=1, da det kun er
registrert et studieprogram med oppstart i 2016.)
 */

 require_once 'db.php';          // get global PDO object
 require_once 'globalFunc.php';  // formats header and bottom

/**
 *  main function, gets first data from studyprogramcontent table and creates table-header
 *  then foreach line call functions to get data from 'subject' table from db
 *  then each of lines gets formatted to fit result table. slitt med dette. to rewrite all
 *  data formatering output....
 *  @method getStudyProgramContent
 */
function getStudyProgramContent() {
  global $db;
  $sql = 'SELECT subject, semester, type FROM studyprogramcontent WHERE startYear=2016 ORDER BY semester';
  $stm = $db->prepare($sql);
  $stm->execute(array());
  $rows = $stm->fetchAll(PDO::FETCH_ASSOC);

  echo pageHeader();
  // opens table-tag and format header, bootstrap class
  echo '<div><table class="table" id="subjectTableHeader"><thead><tr><th>Emnekode</th><th>Emnenavn</th><th>O/V</th><th>Studiepoeng pr semester
        <table style="width:600px"><tbody><tr>
          <td>s1(h)</td>
          <td>s2(v)</td>
          <td>s3(h)</td>
          <td>s4(v)</td>
          <td>s5(h)</td>
          <td>s6(v)</td>
        </tr></tbody></table>
  </th></tr></thead><tbody>';

  foreach ($rows as $row) {
    $subjectData = getSubjectNameByCode($row['subject']);
    $row['name'] = $subjectData['name']; // add subject name from another table
    $row['credits'] = $subjectData['credits'];
    //var_dump($row);  echo '<br></br>';
    echo formatSubjectData($row);
  }
  echo '</tbody></table></div>';  // close table-tags
  echo pageBottom();
}

/**
 *  help function to get individaul data about single subject
 *  @method getSubjectNameByCode
 *  @param  string               $subjectCode f.eks. IMT2291
 *  @return array                assoc.array with code and name
 */
function getSubjectNameByCode($subjectCode) {
  global $db;
  $sql = 'SELECT name, credits FROM subject WHERE code=?';
  $stm = $db->prepare($sql);
  $stm->execute(array($subjectCode));
  $name = $stm->fetch(PDO::FETCH_ASSOC);
  return $name;
}

/**
 *  help function to format HTML of ech result row....to fix better output look
 *  @method formatSubjectData
 *  @param  array            $row all data needed to put in result table
 *  @return HTML
 */
function formatSubjectData($row) {
  if ($row['type']==="obligatory") {  // in case some subjects not obligatory
    $type = "O";
  } else {
    $type = "V";
  }
  $html = '<tr><td>'.$row["subject"].'</td><td>'.$row["name"].'</td><td>'.$type.'</td><td>';
  $html .= '<table style="width:600px"><tbody><tr>';
  $t1 = $t2 = $t3 = $t4 = $t5 = $t6 = '<td>_</td>';
  switch ($row['semester']) {
    case '1': $t1 = '<td>'.$row["credits"].'</td>'; break;
    case '2': $t2 = '<td>'.$row["credits"].'</td>'; break;
    case '3': $t3 = '<td>'.$row["credits"].'</td>'; break;
    case '4': $t4 = '<td>'.$row["credits"].'</td>'; break;
    case '5': $t5 = '<td>'.$row["credits"].'</td>'; break;
    case '6': $t6 = '<td>'.$row["credits"].'</td>'; break;
  }
  $html .= $t1.$t2.$t3.$t4.$t5.$t6;
  $html .= '</tr></tbody></table></td></tr>';
  return $html;
}

/*
main
 */

getStudyProgramContent();

 ?>
