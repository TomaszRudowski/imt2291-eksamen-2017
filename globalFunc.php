<?php
/**
 *  generate html to start a page
 *  @method pageHeader
 *  @return HTML
 */
function pageHeader() {
  $html = '<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Eksamen</title>
    <meta name="author" content="Tomasz Rudowski">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" icrossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="main.css">
  </head><body id="main"><div class="container">';
  return $html;
}

/**
 *  generate html at the end of page (close tags)
 *  @method pageBottom
 *  @return HTML
 */
function pageBottom() {
    $html ='</div></body></html>';
    return $html;
}


 ?>
