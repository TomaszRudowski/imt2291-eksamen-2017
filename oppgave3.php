<?php
/*
Lag et PHP skript (oppgave3.php) som kan brukes for å opprette en ny bruker i
tabellen vi har brukt så langt. Brukeren skal oppgi brukernavn og passord samt
bekrefte passordet. Brukernavn må være minst seks tegn og passordet må være minst åtte tegn.
Begge passord må selvfølgelig være like. Ikke la brukeren sende formen før disse tre betingelsene
er oppfylt. Når brukeren har skrevet korrekt brukernavn og passord skal dataene sendes til samme
skriptet og registreres i databasen. Dersom brukeren kan opprettes (det vil si at det ikke er duplisering
av brukernavn) så setter du sessjonsvariabelen som i oppgave 2 og sender brukeren videre til oppgave2.php.
Dersom brukeren ikke kan opprettes så viser du formen på nytt med brukernavnet fylt ut og en melding til
brukeren om at dette brukernavnet er opptatt og at nytt brukernavn må velges.
 */
require_once 'db.php';  // get global PDO object
require_once 'globalFunc.php';  // formats header and bottom

/**
 *  generate html signup form, using bootstrap classes
 *  @method signupForm
 *  @param  string    $user used to send username if not added successfully
 *  @return HTML
 */
function signupForm($user="") {
  $html = '<div id="signUpformDiv" class="row">
            <form id="signupForm" class="form-register" method="post" action="oppgave3.php">
            <div class="form-group">
              <label for="inputUsername">Username</label>
              <input id="inputUname" class="form-control" value="'.$user.'" name="name" placeholder="Username" required autofocus/>
            </div>
            <div class="form-group">
              <label for="inputPassword" >Password</label>
              <input type="password" id="inputPassword1" class="form-control" name="password1" placeholder="Password" required/>
            </div>
            <div class="form-group">
              <label for="inputPassword" >Retype password</label>
              <input type="password" id="inputPassword2" class="form-control" name="password2" placeholder="Password" required/>
            </div>
            <button name="signupBtn" value="1" class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
            </form>
          </div>
  ';
  return $html;
}

/**
 *  Test if username is allready in use
 *  @method isNameTaken
 *  @param  string      $user username
 *  @return boolean           true - in use, false - available;
 */
function isNameTaken($user) {
  global $db;
  $sql = 'SELECT uname FROM user WHERE uname=? ';
  $stm = $db->prepare($sql);
  $stm->execute(array($user));
  $res = $stm->fetch(PDO::FETCH_ASSOC);
  if ($res) {
    return true;
  } else {
    return false;
  }
}

/**
 *  function tries to add new user, on this stage is both username, and password
 *  verified as unique and correct, according to requirements
 *  @method addNewUser
 *  @param  [type]     $username [description]
 *  @param  [type]     $pwd      [description]
 */
function addNewUser($username, $pwd) {
  try {
    global $db;
    // hashing "password" string
    $hash = password_hash($pwd, PASSWORD_DEFAULT);

    $sql = "INSERT INTO user (uname, pwd) VALUES (?, ?)";
    $stm = $db->prepare($sql);
    $stm->execute(array($username,$hash));
    return $db->lastInsertId();
  } catch(PDOException $e) {
    die ('Unable to add user : '.$e->getMessage());
  }
}

/*
Script runs as follows:
1 - checksif it was run after submit click
  1a - yes
    2 - test if username UNIQUE and 6 char; password at least 8 char and repeated correctly
      2a - yes - start session, set var, header -> oppgave2
      2b - no - reload page with name filled
  1b -no
    2 - shows signup form
 */

if(isset($_POST['signupBtn'])) {    // 1a.
  $username = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $userPwd1 = filter_var($_POST['password1'], FILTER_SANITIZE_STRING);
  $userPwd2 = filter_var($_POST['password2'], FILTER_SANITIZE_STRING);
  if ((strlen($username) < 6) || isNameTaken($username)) {     // 2b.
    echo pageHeader();
    echo "<p>Username: ".$username." not correct (6 char and unique)</p>";
    echo signupForm($username);
    echo pageBottom();
  } else if ($userPwd1 === $userPwd2) { // available name and retyped password match
    if (strlen($userPwd1) < 8) {        // too short password
      echo pageHeader();
      echo "<p>Too short password</p>";
      echo signupForm($username);
      echo pageBottom();
    } else {                        // here all OK, add user open oppgave2.php
      $newUserId = addNewUser($username, $userPwd1);
      session_start();
      $_SESSION['userID'] = $newUserId;
      header("Location: oppgave2.php");
    }
  } else {                          // vailable name and passwords don't match
    echo pageHeader();
    echo "<p>Retyped password doesn't match</p>";
    echo signupForm($username);
    echo pageBottom();
  }
} else {                            // 1b.
  echo pageHeader();
  echo signupForm();
  echo pageBottom();
}

 ?>
