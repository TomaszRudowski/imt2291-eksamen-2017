<?php
/*
I oppgave5.php skal det generes en liste over alle registrerte brukere. Dersom
brukeren har lastet opp et avatarbilde skal dette også vises. Bruk Bootstrap for
å formatere dataene slik at listen kan vises på alt fra store skjermer til mobil.
Lag skriptet avatar.php for å hente bilder fra databasen slik at disse kan vises.
 */
 require_once 'db.php';  // get global PDO object
 require_once 'globalFunc.php';  // formats header and bottom


/**
 *  Script starts with getting all id's of registered users
 *  @method getAllUserID
 *  @return array       array with users id
 */
function getAllUserID() {
  global $db;
  $sql = 'SELECT id FROM user';
  $stm = $db->prepare($sql);
  $stm->execute(array());
  $rows = $stm->fetchAll(PDO::FETCH_ASSOC);
  $res = array();
  foreach ($rows as $row) {
    array_push($res, $row['id']);
  }
  return $res;
}

/**
 *  Formats html. Gets avatar picture if available, or 'no picture' text
 *  @method getUserPicture
 *  @param  string         $id userId
 *  @return HTML
 */
function getUserPicture($id) {
  global $db;
  $sql = 'SELECT avatar FROM user WHERE id=?';
  $stm = $db->prepare($sql);
  $stm->execute(array($id));
  $row = $stm->fetch(PDO::FETCH_ASSOC);
  if ($row['avatar']) {   // there is a picture
    // Used this one line from: https://stackoverflow.com/questions/3695077/php-html-image-output
    // to format binary string to picture and return img-tag
    $res =  '<img src="data:image/png;base64,'.base64_encode($row['avatar']).'" />';
  } else {
    $res= '<p>no picture</p>';
  }
  return $res;
}

/**
 *  Gets username form db. returns as a string
 *  @method getUserName
 *  @param  string         $id userId
 *  @return string         username
 */
function getUserName($id) {
  global $db;
  $sql = 'SELECT uname FROM user WHERE id=? ';
  $stm = $db->prepare($sql);
  $stm->execute(array($id));
  $row = $stm->fetch(PDO::FETCH_ASSOC);
  return $row['uname'];
}

/**
 *  main function to format a table with results. Can work a bit on formating...
 *  @method generateTable
 *  @return HTML
 */
 function generateTable() {
   $html = '<div >';
   $userIdList = getAllUserID();
   foreach ($userIdList as $index=>$userId) {
    $html .= '<div class="row">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">'.
    getUserPicture($userId).'</div>
          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">'.
            getUserName($userId)
          .'</div>
        </div>';
   }


   $html .= '</div>';  // close container
   return $html;
 }


/*
script works as follows. Gets all ID (only) and get picture and name by separate
functions called in each/proper cells i the table
 */
echo pageHeader();
echo generateTable();
echo pageBottom();

 ?>
