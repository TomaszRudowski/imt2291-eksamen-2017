<?php
/* Lag et PHP skript (oppgave1.php) som oppretter en database med navn "IMT2291Eksamenstudentnr"
(altså ditt studentnummer til slutt i navnet til databasen), dette er den databasen du vil bruke gjennom hele oppgavesettet.
I denne tabellen skal skriptet videre opprette en tabell "user" med kolonnene :
id: bigint, primary key, auto increment, not null
uname: varchar 32, unique, not null
pwd: varchar 255, not null
avatar: blob
Legg til en bruker med brukernavn "test" og passord "test". Passordet skal hashes med php funksjonen password_hash.*/

/** user "root" uten password used to get PDO object(root for local db). Then I create db.
 *
 *  @method createLocalDatabase
 */
function createLocalDatabase() {
	try {
		$db = new PDO("mysql:host=localhost;", "root", "");
		$sql = "CREATE DATABASE IMT2291Eksamen471181 COLLATE='utf8_danish_ci'";
		$res = $db->exec($sql);
    // for testing
		// echo "$res <br> Database created successfully<br>";
    // here comes table, command USE to presise which db I'm using
    $sql = "USE IMT2291Eksamen471181; CREATE TABLE user (
      id BIGINT AUTO_INCREMENT PRIMARY KEY,
      uname VARCHAR(32) UNIQUE NOT NULL,
      pwd VARCHAR(255) NOT NULL,
      avatar BLOB
    );";
    $res = $db->exec($sql);
    // for testing
		// echo "$res <br> Tables created successfully<br>";

    // add test user

	} catch(PDOException $e) {
		die ('Unable to create to database : '.$e->getMessage());
	}
}

/**
 *  Function opens connection to newly created db and inserts one row in user table
 *  @method addTestUser
 */
function addTestUser() {
  try {
    // here I can open the db that was created
    $db = new PDO("mysql:host=localhost;dbname=IMT2291Eksamen471181", "root", "");
    // hashing "test" string
    $hash = password_hash("test", PASSWORD_DEFAULT);
    $user = "test";
    // here I'm using prepare statement
    $sql = "INSERT INTO user (uname, pwd) VALUES (?, ?)";
    $stm = $db->prepare($sql);
    $stm->execute(array($user,$hash));
  } catch(PDOException $e) {
    die ('Unable to create to add user : '.$e->getMessage());
  }
}

/*
main program, runs two functions to create db and insert test user
 */
createLocalDatabase();
addTestUser();

 ?>
