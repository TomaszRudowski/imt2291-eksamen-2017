<?php
/*
Lag et PHP skript (oppgave2.php) som første gang viser en form hvor brukeren skal
 skrive inn brukernavn og passord (bruk <label> for å gi ledetekst til feltene).
 Bruk CSS for å gi fornuftig formatering til formen. Når brukeren trykker på knappen
 for å logge på skal brukernavn og passord sendes til samme skriptet. Sjekk om brukeren
 finnes i databasen. Dersom brukeren ikke finnes i databasen så vis formen på nytt med
 brukernavnet  fylt ut men uten passordet fylt ut. Gi brukeren tilbakemelding om
 at bruker med oppgitt brukernavn/passord kombinasjon ikke finnes i databasen.
Dersom brukeren finnes skal det settes en sessjonsvariabel med id'en til brukeren.
Videre skal det vises en velkomstmelding med brukernavnet til brukeren samt en knapp
for å kunne logge ut.
Dersom denne siden (oppgave2.php) vises mens en bruker er logget på skal også denne
meldingen og knappen vises. Du må altså sjekke sessjonsvariabel for å finne ut om
formen eller denne meldingen skal vises.
Når brukeren trykker på knappen for å logge ut skal igjen samme skriptet kalles og
sessjonsvariabelen slettes. Brukeren skal da få frem formen for å fylle inn brukernavn og passord.
 */
require_once 'db.php';  // get global PDO object
require_once 'globalFunc.php';  // formats header and bottom

session_start();


/**
 *  generate html login form, using bootstrap classes
 *  @method loginForm
 *  @param  string    $user used to send username if not logged in successfully
 *  @return HTML
 */
function loginForm($user="") {
  $html = '<div id="formDiv" class="row">
            <form id="loginForm" class="form-register" method="post" action="oppgave2.php">
            <div class="form-group">
              <label for="inputUsername">Username</label>
              <input id="inputUname" class="form-control" value="'.$user.'" name="name" placeholder="Username" required autofocus/>
            </div>
            <div class="form-group">
              <label for="inputPassword" >Password</label>
              <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required/>
            </div>
            <button name="lbtn" value="1" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
          </div>
  ';
  return $html;
}

/**
 *  Formats html to show welcome message and log out button
 *  @method velcomePage
 *  @param  string      $user Username
 *  @return HTML
 */
function velcomePage($user) {
  $html = '<p>Welcome '.$user.'</p>
          <p>
            <form id="logoutForm" class="form-register" method="get" action="oppgave2.php">
              <button name="logout" value="true" class="btn btn-lg btn-primary btn-block" type="submit">Sign out</button>
            </form>
          </p>
  ';
  return $html;
}

/**
 *  receive username and password and check them towards db
 *  @method testLogin
 *  @param  string    $user name from $_POST
 *  @param  string    $pwd  password from $_POST
 *  @return string          user ID or NULL
 */
function testLogin($user, $pwd) {
  global $db;
  $sql = 'SELECT id, pwd FROM user WHERE uname=? ';
  $stm = $db->prepare($sql);
  $stm->execute(array($user));
  $res = $stm->fetch(PDO::FETCH_ASSOC);
  if (password_verify($pwd, $res['pwd'])) {
    return $res['id'];
  } else {
    return NULL;
  }
}

/**
 *  when session used i need to get username that corresponds with ID
 *  @method getUserNameById
 *  @param  string          $id user id from $_SESSION
 *  @return string          username as in db
 */
function getUserNameById($id) {
  global $db;
  $sql = 'SELECT uname FROM user WHERE id=? ';
  $stm = $db->prepare($sql);
  $stm->execute(array($id));
  $res = $stm->fetch(PDO::FETCH_ASSOC);
  return $res['uname'];
}

/* script runs in four possible ways:
1-after logout
2- after submit loginForm
2a - correct
2b - incorrect
3 - session var used
4 - normal, first time opened
*/

if (isset($_GET['logout'])) {  // "sign out" clicked
  // remove session variable, show default sign in form
  unset ($_SESSION['userID']);
  //var_dump($_SESSION);
  //session_destroy();
  header("Location: oppgave2.php");
} else {
  if (isset($_POST['lbtn'])) { // "sign in" clicked
    $username = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
    $userPwd = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
    // test if correct user/password
    $userID = testLogin($username, $userPwd);
    if ($userID) {  // user Id or NULL returned
      // yes: hide signin form; set session variable; show velcome messg and "sign out" button

      $_SESSION['userID'] = $userID;
      echo pageHeader();
      echo velcomePage($username);
      echo pageBottom();
    } else {                              // name and password don't match
      // no: show signin form with user name filled
      echo pageHeader();
      echo loginForm($username);
      echo pageBottom();
    }
  } else if (isset($_SESSION['userID'])) {  // close and open without loging out
    $userById = getUserNameById(($_SESSION['userID']));
    //var_dump($_SESSION);
    echo pageHeader();
    echo 'using session';
    echo velcomePage($userById);
    echo pageBottom();
  } else {                              // none of previous, clear login page
    echo pageHeader();
    echo loginForm();
    echo pageBottom();
  }
}

?>
