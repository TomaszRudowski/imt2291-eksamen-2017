<?php
/*
oppgave6.php skal returnere en liste over alle brukere på JSON format. Det som skal
returneres for hver bruker er brukerid, brukernavn og "y"/"n" for hvorvidt det finnes
et avatarbilde eller ikke. JSON dataene skal returneres med Content-type = application/json.
 */
 require_once 'db.php';  // get global PDO object
 require_once 'globalFunc.php';  // formats header and bottom

/**
 *  gets data from db. for each row/user copies 'id' and 'uname' and based on
 *  'avatar' sets y/n. Then creates array of user-arrays. At the end encodes as JSON
 *  Need to echo results (JSON object) towards frontend.
 *  @method getUserDataAsJSON
 *  @return JSON            set of users with relevant data
 */
 function getUserDataAsJSON() {
   global $db;
   $sql = 'SELECT id, uname, avatar FROM user';
   $stm = $db->prepare($sql);
   $stm->execute(array());
   $rows = $stm->fetchAll(PDO::FETCH_ASSOC);
   $allUsers = array(); // set of all users
   foreach ($rows as $row) {
    $oneUser = array();
    $oneUser['id'] = $row['id'];
    $oneUser['uname'] = $row['uname'];
    if ($row['avatar']) {     // used to check 'null' or 'binary string'
      $oneUser['avatar'] = 'y';
    } else {
      $oneUser['avatar'] = 'n';
    }
    array_push($allUsers, $oneUser);  // add current user to set
   }
   return json_encode($allUsers);
 }

/*
main skript
 */
echo getUserDataAsJSON();

 ?>
